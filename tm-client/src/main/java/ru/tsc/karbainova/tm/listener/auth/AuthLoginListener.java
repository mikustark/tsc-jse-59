package ru.tsc.karbainova.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;
import ru.tsc.karbainova.tm.listener.TerminalUtil;

@Component
public class AuthLoginListener extends AbstractSystemListener {
    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login app";
    }

    @Override
    @EventListener(condition = "@authLoginListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        SessionDTO session = sessionEndpoint.openSession(login, password);
        sessionService.setSession(session);

        System.out.println("[OK]");
    }
}
