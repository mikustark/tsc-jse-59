package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.listener.AbstractListener;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CommandRepository implements ICommandRepository {

    @Nullable
    private final Map<String, AbstractSystemListener> arguments = new LinkedHashMap<>();
    @NonNull
    private final Map<String, AbstractListener> commands = new LinkedHashMap<>();

    @Override
    public AbstractListener getCommandByName(@NonNull String name) {
        return commands.get(name);
    }

    @Override
    public AbstractListener getCommandByArg(@Nullable String arg) {
        return commands.get(arg);
    }

    @NonNull
    @Override
    public Map<String, AbstractListener> getCommands() {
        return commands;
    }

    @Nullable
    @Override
    public Map<String, AbstractSystemListener> getArguments() {
        return arguments;
    }

    @Override
    public Collection<String> getCommandArg() {
        final List<String> result = new ArrayList<>();
        for (final AbstractSystemListener command : arguments.values()) {
            final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandName() {
        final List<String> result = new ArrayList<>();
        for (final AbstractListener command : commands.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public void create(AbstractSystemListener command) {
        final String name = command.name();
        final String arg = command.arg();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

    @Override
    public void add(@NotNull AbstractListener command) {
        if (command instanceof AbstractSystemListener) {
            @NotNull final AbstractSystemListener systemCommand;
            systemCommand = (AbstractSystemListener) command;
            @Nullable final String arg = systemCommand.arg();
            if (arg != null) arguments.put(arg, systemCommand);
        }
        @Nullable final String name = command.name();
        if (name != null) commands.put(name, command);
    }
}
