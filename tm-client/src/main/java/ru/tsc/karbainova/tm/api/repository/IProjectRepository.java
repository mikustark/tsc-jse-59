package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.endpoint.ProjectDTO;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<ProjectDTO> {

    void add(String userId, ProjectDTO project);

    void remove(String userId, ProjectDTO project);

    boolean existsById(String userId, String id);

    List<ProjectDTO> findAll();

    List<ProjectDTO> findAll(String userId);

    List<ProjectDTO> findAll(String userId, Comparator<ProjectDTO> comparator);

    void clear();

    void clear(String userId);

    ProjectDTO findById(String userId, String id);

    ProjectDTO findByIndex(String userId, int index);

    ProjectDTO findByName(String userId, String name);

    ProjectDTO removeById(String userId, String id);

    ProjectDTO removeByName(String userId, String name);

    ProjectDTO removeByIndex(String userId, int index);


}
