package ru.tsc.karbainova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.SessionDTO;

import java.util.List;

public interface ISessionDTORepository extends IDTORepository<SessionDTO> {
    void clear();

    @NotNull List<SessionDTO> findAll();

    @NotNull List<SessionDTO> findAllByUserId(@NotNull String userId);

    @Nullable SessionDTO findById(@Nullable String id);

    void removeById(@Nullable String id);

    void removeByUserId(@NotNull String userId);

    int getCount();
}
