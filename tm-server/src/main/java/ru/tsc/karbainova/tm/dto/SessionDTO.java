package ru.tsc.karbainova.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.tsc.karbainova.tm.listener.JpaEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Entity
@Table(name = "tm_session")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDTO extends AbstractDTOEntity implements Cloneable {

    public SessionDTO() {
    }

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Getter
    @Setter
    @Column
    private Long timestamp;

    @Getter
    @Setter
    @Column(name = "user_id")
    private String userId;

    @Getter
    @Setter
    @Column
    private String signature;


}
