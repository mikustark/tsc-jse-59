package ru.tsc.karbainova.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.ILoggerService;
import ru.tsc.karbainova.tm.dto.LogDto;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

@Component
public class JmsLogListener implements MessageListener {

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
        if (entity instanceof LogDto) loggerService.writeLog((LogDto) entity);
    }

}
