package ru.tsc.karbainova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.UserDTO;

import java.util.List;

public interface IUserDTORepository extends IDTORepository<UserDTO> {
    void clear();

    List<UserDTO> findAll();

    @Nullable UserDTO findById(@Nullable String id);

    @Nullable UserDTO findByLogin(@NotNull String login);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    int getCount();
}
