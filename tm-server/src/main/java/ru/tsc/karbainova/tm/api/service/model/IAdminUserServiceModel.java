package ru.tsc.karbainova.tm.api.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IAdminUserServiceModel {
    @SneakyThrows
    User removeUser(@Nullable User user);

    @SneakyThrows
    User lockUserByLogin(@NonNull String login);

    @SneakyThrows
    User unlockUserByLogin(@NonNull String login);

    @SneakyThrows
    List<User> findAll();

    @SneakyThrows
    void addAll(Collection<User> collection);

    @SneakyThrows
    User findByLogin(@NonNull String login);

    @SneakyThrows
    User create(@NonNull String login, @NonNull String password);

    @SneakyThrows
    User create(@NonNull String login, @NonNull String password, @NonNull Role role);

    @SneakyThrows
    User create(@NonNull String login, @NonNull String password, @NonNull String email);

    @SneakyThrows
    User setPassword(@NonNull String userId, @NonNull String password);

    @SneakyThrows
    User updateUser(
            @NonNull String userId,
            @NonNull String firstName,
            @NonNull String lastName,
            @Nullable String middleName);

    @SneakyThrows
    void clear();

    @SneakyThrows
    User add(User user);
}
