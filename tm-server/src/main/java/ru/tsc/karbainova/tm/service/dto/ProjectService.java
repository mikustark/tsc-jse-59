package ru.tsc.karbainova.tm.service.dto;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.karbainova.tm.api.service.dto.IProjectService;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService<ProjectDTO> implements IProjectService, ru.tsc.karbainova.tm.api.service.dto.IOwnerService<ProjectDTO> {

    @NotNull
    @Autowired
    private IProjectDTORepository projectRepository;

    @Override
    @Transactional
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Transactional
    @SneakyThrows
    public void clear() {
        projectRepository.clear();
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<ProjectDTO> collection) {
        if (collection == null) return;
        for (ProjectDTO i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public ProjectDTO add(ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
        return project;
    }

    @Override
    @Transactional
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        projectRepository.add(project);
    }


    @Override
    @Transactional
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
    }


    @Override
    @Transactional
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.removeByIdAndUserId(userId, project.getId());
    }

    @Override
    @Transactional
    @SneakyThrows
    public ProjectDTO updateById(@NonNull String userId, @NonNull String id,
                                 @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final ProjectDTO project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(description);
        projectRepository.update(project);
        return project;
    }

    @Override
    @Transactional
    @NonNull
    @SneakyThrows
    public ProjectDTO findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }
}
