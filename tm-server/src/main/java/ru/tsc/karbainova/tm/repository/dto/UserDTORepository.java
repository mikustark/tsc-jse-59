package ru.tsc.karbainova.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.karbainova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.karbainova.tm.dto.UserDTO;

import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDTO e")
                .executeUpdate();
    }

    @Override
    public List<UserDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM UserDTO e", UserDTO.class).getResultList();
    }

    @Override
    public UserDTO findById(@Nullable final String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Override
    public UserDTO findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@Nullable final String id) {
        UserDTO reference = entityManager.getReference(UserDTO.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserDTO e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM UserDTO e", Long.class)
                .getSingleResult()
                .intValue();
    }

}

