package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.api.service.dto.ISessionService;
import ru.tsc.karbainova.tm.api.service.dto.IUserService;
import ru.tsc.karbainova.tm.component.Bootstrap;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.AccessDeniedException;
import ru.tsc.karbainova.tm.exception.AccessForbiddenException;
import ru.tsc.karbainova.tm.exception.empty.EmptyLoginException;
import ru.tsc.karbainova.tm.exception.empty.EmptyPasswordException;
import ru.tsc.karbainova.tm.exception.empty.EmptySessionNlException;
import ru.tsc.karbainova.tm.exception.empty.EmptyUserNotFoundException;
import ru.tsc.karbainova.tm.service.PropertyService;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SessionService extends AbstractService<SessionDTO> implements ISessionService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISessionDTORepository sessionRepository;


    @Override
    public boolean checkDataAccess(String login, String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        ServiceLocator serviceLocator = new Bootstrap();
        final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new EmptyUserNotFoundException();
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @Transactional
    @SneakyThrows
    public SessionDTO add(SessionDTO session) {
        if (session == null) throw new EmptySessionNlException();
        
        sessionRepository.add(session);
        return session;
    }

    @Override
    @Transactional
    @SneakyThrows
    public SessionDTO open(String login, String password) {
//        final boolean check = checkDataAccess(login, password);
//        if (!check) throw new EmptyLoginOrPasswordException();
//        if (check) throw new EmptyLoginOrPasswordException();
        ServiceLocator serviceLocator = new Bootstrap();
        final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new EmptyLoginException();
        final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        
        sign(session);
        sessionRepository.add(session);
        return session;
    }

    @Override
    public SessionDTO sign(SessionDTO session) {
        if (session == null) throw new EmptySessionNlException();
        session.setSignature(null);
        IPropertyService propertyService = new PropertyService();
        final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public List<SessionDTO> findAll() {
        
        return sessionRepository.findAll();
    }

    @Override
    public List<SessionDTO> getListSessionByUserId(String userId) {
        return findAll()
                .stream()
                .filter(s -> s.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public void validate(SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NonNull final String signatureSource = session.getSignature();
        @NonNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        
        if (sessionRepository.findById(session.getId()) == null) throw new AccessDeniedException();
    }

    @Override
    public void validate(SessionDTO session, Role role) {
        if (role == null) throw new AccessForbiddenException();
        validate(session);
        final String userId = session.getUserId();
        ServiceLocator serviceLocator = new Bootstrap();
        final UserDTO user = userService.findById(userId);
        if (user == null) throw new AccessForbiddenException();
        if (user.getRole() == null) throw new AccessForbiddenException();
        if (!role.equals(user.getRole())) throw new AccessForbiddenException();
    }

    @Override
    @Transactional
    @SneakyThrows
    public void close(SessionDTO session) {
        
        sessionRepository.removeById(session.getId());
    }

    @Override
    @Transactional
    public void closeAll(SessionDTO session) {
        validate(session);
        List<SessionDTO> sessions = findAll().stream().filter(s -> s.getUserId().equals(session.getUserId())).collect(Collectors.toList());
        sessions.forEach(this::close);
    }

    @Override
    @Transactional
    public void signOutByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return;
        List<SessionDTO> sessions = findAll().stream().filter(s -> s.getUserId().equals(userId)).collect(Collectors.toList());
        sessions.forEach(this::close);
    }
}
