package ru.tsc.karbainova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {
    void clear();

    List<User> findAll();

    User findById(@Nullable String id);

    User findByLogin(@NotNull String login);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    int getCount();
}
