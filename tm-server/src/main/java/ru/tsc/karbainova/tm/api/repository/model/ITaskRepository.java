package ru.tsc.karbainova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {
    void clear();

    void clearByUserId(@NotNull String userId);

    Task findById(@Nullable String id);

    Task findByName(@Nullable String userId, @Nullable String name);

    void removeById(@Nullable String id);

    void remove(@NotNull Task entity);

    List<Task> findAll();

    List<Task> findAllByUserId(@Nullable String userId);

    Task findByIdUserId(@Nullable String userId, @Nullable String id);

    Task findByIndex(@Nullable String userId, @NotNull Integer index);

    void removeByName(@Nullable String userId, @Nullable String name);

    void removeByIdUserId(@Nullable String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, int index);

    int getCount();

    int getCountByUser(@NotNull String userId);

    void bindTaskById(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void unbindTaskById(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @NotNull List<Task> findTasksByUserIdProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void removeTasksByProjectId(@Nullable String projectId);
}
