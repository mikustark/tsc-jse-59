package ru.tsc.karbainova.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.karbainova.tm.api.service.IPropertyService;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.karbainova.tm")
public class ContextConfiguration {
    @Bean
    public DataSource dataSource(@NotNull final IPropertyService propertyService) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getJdbcDriver());
        dataSource.setUrl(propertyService.getJdbcUrl());
        dataSource.setUsername(propertyService.getJdbcUser());
        dataSource.setPassword(propertyService.getJdbcPassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final IPropertyService propertyService,
            final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                "ru.tsc.karbainova.tm.model",
                "ru.tsc.karbainova.tm.dto"
        );
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.SHOW_SQL, propertyService.getSqlShow());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getAuto());
        properties.put(Environment.DIALECT, propertyService.getDialect());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }
}
